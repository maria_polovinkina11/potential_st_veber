﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Potential_St_Veber_v21
{
    /// <summary>
    /// Потенциал взаимодействия.
    /// </summary>
    public abstract class Potential
    {

        public abstract double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false);
    }


    public class PotentialStillinger : Potential
    {
        /// <summary>
        /// Параметры потенциала Стиллинджера-Вебера.
        /// </summary>
        private struct ParamPotential
        {
            public double A, B, p, a, l, y, q, s, e;
            public ParamPotential(double A, double B, double p, double a, double l, double y, double q, double s, double e)
            {
                this.A = A;
                this.B = B;
                this.p = p;
                this.a = a;//сигма
                this.l = l;//лямбда
                this.y = y;//гамма
                this.q = q;
                this.s = s;
                this.e = e;
            }


        }

        /// <summary>
        /// Параметры для различных соединений атомов.
        /// </summary>

        private ParamPotential paramOfSi, paramOfGe, paramOfSn;
        public PotentialStillinger(double latParSi, double latParGe, double latParSn, double latStruct)
        {


            double si = 0.2095;
            double ge = 0.2181;
            double sn = 0.2492;
            //latParSn * Math.Sqrt(3) / (4 * Math.Pow(2, 1 / 6.0));


            paramOfSi = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * si, 21.0, 1.20, 0, si, 2.16); //2.1672381
            paramOfGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * ge, 31.0, 1.20, 0, ge, 1.93);
            paramOfSn = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * sn, 21.0, 1.20, 0, sn, 1.56);
        }

        private ParamPotential AngleFunction(AtomType at1, AtomType at2, AtomType at3)
        {
            if (at1 == AtomType.Si && at2 == AtomType.Si && at3 == AtomType.Si)
            {
                return paramOfSi;
            }

            if (at1 == AtomType.Ge && at2 == AtomType.Ge && at3 == AtomType.Ge)
            {
                return paramOfGe;
            }

            if (at1 == AtomType.Sn && at2 == AtomType.Sn && at3 == AtomType.Sn)
            {
                return paramOfSn;
            }

            return new ParamPotential(0, 0, 0, 0, 0, 0, 0, 0, 0);
        }

        /// <summary>
        /// Функция двухчастичного взаимодействия ф(r).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="radius">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius > potential.a) return 0.0;
            else return potential.e * potential.A * (potential.B * Math.Pow(potential.s / radius, potential.p) - Math.Pow(potential.s / radius, potential.q))
                    * Math.Exp(potential.s / (radius - potential.a));
        }

        /// <summary>
        /// Затухающая функция, критический радиус которой находится между первым и вторым ближайшим соседом.
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="Rij">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionH(ParamPotential potentialIJ, ParamPotential potentialIK, double Rij, double Rik)
        {
            if (Rij > potentialIJ.a) return 0.0;
            else if (Rik > potentialIK.a) return 0.0;
            else
            {
                double exponenta = (potentialIJ.y * potentialIJ.s / (Rij - potentialIJ.a)) + (potentialIK.y * potentialIK.s / (Rik - potentialIK.a));
                return Math.Exp(exponenta);
            }
        }

        /// <summary>
        /// Функция (cos + 1/3)^2.
        /// </summary>
        /// <param name="Rij">Расстояние до первого атома-соседа.</param>
        /// <param name="Rik">Расстояние до второго атома-соседа.</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом.</param>
        /// <returns></returns>
        private double PE_FunctionCos(double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов.
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);
            return Math.Pow((1.0 / 3.0) + cosTeta, 2);
        }

        /// <summary>
        /// Потенциальная энергия выбранного атома.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <param name="force">Расчёт энергии взаимодействия выбранного атома.</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false)
        {
            double atomEnergyDuo = 0.0, atomEnergyTrio = 0.0;


            for (int j = 0; j < selAtom.Neighbours.Length; j++)
            {
                double Rij = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[j].Coordinate, lengthSystem);

                ParamPotential potentialIJ = paramOfSi;
                if (selAtom.Type == AtomType.Si && selAtom.Neighbours[j].Type == AtomType.Si) potentialIJ = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[j].Type == AtomType.Ge) potentialIJ = paramOfGe;
                if (selAtom.Type == AtomType.Sn && selAtom.Neighbours[j].Type == AtomType.Sn) potentialIJ = paramOfSn;



                atomEnergyDuo += PE_FunctionCutoff(potentialIJ, Rij);

                for (int k = 0; k < selAtom.Neighbours.Length; k++)
                {
                    if (k == j) continue;
                    double Rik = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);
                    double Rjk = Vector.MagnitudePeriod(selAtom.Neighbours[j].Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);

                    double cosTetaJIK = PE_FunctionCos(Rij, Rik, Rjk);
                    double cosTetaIJK = PE_FunctionCos(Rij, Rjk, Rik);
                    double cosTetaIKJ = PE_FunctionCos(Rik, Rjk, Rij);

                    int typeSi = 0;
                    int typeGe = 0;
                    int typeSn = 0;

                    if (selAtom.Type == AtomType.Si) ++typeSi;
                    if (selAtom.Neighbours[j].Type == AtomType.Si) ++typeSi;
                    if (selAtom.Neighbours[k].Type == AtomType.Si) ++typeSi;

                    if (selAtom.Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[k].Type == AtomType.Ge) ++typeGe;

                    if (selAtom.Type == AtomType.Sn) ++typeSn;
                    if (selAtom.Neighbours[j].Type == AtomType.Sn) ++typeSn;
                    if (selAtom.Neighbours[k].Type == AtomType.Sn) ++typeSn;

                    ParamPotential potentialIJK = paramOfSi;

                    if (typeSi == 3) potentialIJK = paramOfSi;
                    else if (typeSi == 2) potentialIJK = paramOfSi;
                    else if (typeSi == 1) potentialIJK = paramOfSi;

                    if (typeGe == 3) potentialIJK = paramOfGe;
                    else if (typeGe == 2) potentialIJK = paramOfGe;
                    else if (typeGe == 1) potentialIJK = paramOfGe;

                    if (typeSn == 3) potentialIJK = paramOfSn;
                    else if (typeSn == 2) potentialIJK = paramOfSn;
                    else if (typeSn == 1) potentialIJK = paramOfSn;


                    ParamPotential potentialIK = paramOfSi;
                    if (selAtom.Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialIK = paramOfSi;
                    if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialIK = paramOfGe;
                    if (selAtom.Type == AtomType.Sn && selAtom.Neighbours[k].Type == AtomType.Sn) potentialIK = paramOfSn;

                    ParamPotential potentialJK = paramOfSi;
                    if (selAtom.Neighbours[j].Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialJK = paramOfSi;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialJK = paramOfGe;
                    if (selAtom.Neighbours[j].Type == AtomType.Sn && selAtom.Neighbours[k].Type == AtomType.Sn) potentialJK = paramOfSn;

                    atomEnergyTrio += potentialIJK.e * potentialIJK.l * (PE_FunctionH(potentialIJ, potentialIK, Rij, Rik) * cosTetaJIK
                    + PE_FunctionH(potentialIJ, potentialJK, Rij, Rjk) * cosTetaIJK + PE_FunctionH(potentialIK, potentialJK, Rik, Rjk) * cosTetaIKJ);
                }
            }

            double atomEnergy = 0.0;
            if (!force) atomEnergy = atomEnergyDuo / 2.0 + atomEnergyTrio / 3.0;
            else atomEnergy = atomEnergyDuo + atomEnergyTrio;

            return atomEnergy;
        }
    }
}

        