﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Potential_St_Veber_v21
{
    public partial class FormOptions : Form
    {
        public FormOptions()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Параметры моделирования.
        /// </summary>
        public struct ModelingParams
        {
            public int seedCreation;
            public int seedShift;
            public bool minMainForm;

            /// <summary>
            /// Инициализация параметров.
            /// </summary>
            /// <param name="seedCreation">Затравка создания структуры.</param>
            /// <param name="seedShift">Затравка начального сдвига структуры.</param>
            /// <param name="minMainForm">Сворачивание главного окна после запуска моделирования.</param>
            public ModelingParams(int seedCreation, int seedShift, bool minMainForm)
            {
                this.seedCreation = seedCreation;
                this.seedShift = seedShift;
                this.minMainForm = minMainForm;
            }
        }
        /// <summary>
        /// Получить изменённые настройки моделирования.
        /// </summary>
        public ModelingParams GetModelParams
        {
            get { return modelParams; }
        }
        private ModelingParams modelParams;

        /// <summary>
        /// Создание окна настроек.
        /// </summary>
        /// <param name="modelParams">Настройки моделирования по умолчанию.</param>
        public FormOptions(ModelingParams modelParams)
        {
            InitializeComponent();

            this.modelParams = modelParams;

            InitializeFields(modelParams);
        }

        /// <summary>
        /// Проинициализировать поля.
        /// </summary>
        /// <param name="modelParams"></param>
        private void InitializeFields(ModelingParams modelParams)
        {
            numUD_randCreate.Value = modelParams.seedCreation;
            numUD_randShift.Value = modelParams.seedShift;
            check_minMainForm.Checked = modelParams.minMainForm;
        }

        /// <summary>
        /// Получить значения полей.
        /// </summary>
        /// <returns></returns>
        private ModelingParams GetFieldValues()
        {
            return new ModelingParams((int)numUD_randCreate.Value, (int)numUD_randShift.Value, check_minMainForm.Checked);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void FormOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            modelParams = GetFieldValues();
        }
    }
}
