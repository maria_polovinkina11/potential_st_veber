﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Potential_St_Veber_v21
{


    public partial class Structure
    {

        /// <summary>
        /// Потенциал Стиллинджера-Вебера.
        /// </summary>
        private PotentialStillinger pStillinger;

        /// <summary>
        /// Выбранный потенциал.
        /// </summary>
        public Potential SelPotential
        {
            private set { selPotential = value; }
            get { return selPotential; }
        }
        private Potential selPotential;

        /// <summary>
        /// Кубическая кристаллическая решётка.
        /// </summary>
        public readonly Atom[] Struct;

        /// <summary>
        /// Количество атомов в элементарной атомной ячейке.
        /// </summary>
        public const int NumberAtoms = 8;

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        public readonly int StructNumAtoms;

        /// <summary>
        /// Размер структуры.
        /// </summary>
        public readonly int StructLength;

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        public readonly double StructLatPar;

        /// <summary>
        /// Создание кубической структуры.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        public Structure(int length, bool Si, bool Ge, bool Sn)
        {
            // Задание параметров.
            this.StructLength = length;
            this.StructNumAtoms = TotalNumAtoms(this.StructLength);
            this.StructLatPar = AtomLatticeParam(Si, Ge, Sn);
            this.Struct = new Atom[this.StructNumAtoms];

            this.pStillinger = new PotentialStillinger(AtomParams.LATPAR_SI, AtomParams.LATPAR_GE, AtomParams.LATPAR_SN, this.StructLatPar);
            this.SetPotential();

            AtomType setType = AtomType.Si;
            if (Ge == true)
            {
                setType = AtomType.Ge;
            }
            if (Sn == true)
            {
                setType = AtomType.Sn;
            }




            // Размещение атомов.
            int atomIdx = -1;
            for (int x = 0; x < this.StructLength; ++x)
                for (int y = 0; y < this.StructLength; ++y)
                    for (int z = 0; z < this.StructLength; ++z)
                    {
                        double koeff = 0.25d * this.StructLatPar;

                        Vector posCell = new Vector(x, y, z) * this.StructLatPar;

                        this.Struct[++atomIdx] = new Atom(posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(0, 2, 2) * koeff + posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(2, 0, 2) * koeff + posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(2, 2, 0) * koeff + posCell, setType, atomIdx);

                        this.Struct[++atomIdx] = new Atom(new Vector(1, 1, 1) * koeff + posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(1, 3, 3) * koeff + posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(3, 1, 3) * koeff + posCell, setType, atomIdx);
                        this.Struct[++atomIdx] = new Atom(new Vector(3, 3, 1) * koeff + posCell, setType, atomIdx);
                    }


            this.SearchNeighbours();
        }

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        /// <returns></returns>
        public static int TotalNumAtoms(int length)
        {
            return length * length * length * NumberAtoms;
        }

        /// <summary>
        /// Потенциал взаимодействия.
        /// </summary>
        public void SetPotential()
        {
            this.SelPotential = pStillinger;
        }

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        /// <returns></returns>
        public static double AtomLatticeParam(bool Si, bool Ge, bool Sn)
        {
            double Param = 0;
            if (Si == true && Ge == false && Sn == false)
            {
                Param = AtomParams.LATPAR_SI;
            }
            if (Si == false && Ge == true && Sn == false)
            {
                Param = AtomParams.LATPAR_GE;
            }
            if (Si == false && Ge == false && Sn == true)
            {
                Param = AtomParams.LATPAR_SN;
            }
            return Param;
        }

        /// <summary>
        /// Поиск соседей у всех атомов структуры.
        /// </summary>
        public void SearchNeighbours()
        {
            double L = this.StructLength * this.StructLatPar;
            double radiusOfNeighbours = new Vector(this.StructLatPar * 0.25d, this.StructLatPar * 0.25d, this.StructLatPar * 0.25d).Magnitude();
            radiusOfNeighbours += radiusOfNeighbours * 0.2d;

            for (int i = 0; i < this.StructNumAtoms; i++)
            {
                this.Struct[i].SearchFirstNeighbours(this.Struct, L, radiusOfNeighbours);
            }

            for (int j = 0; j < this.StructNumAtoms; j++)
            {
                this.Struct[j].SearchSecondNeighbours();
                this.Struct[j].SumNeighbours();
            }
        }

        /// <summary>
        /// Подсчёт полной потенциальной энергии системы.
        /// </summary>
        /// <returns></returns>
        public double PotentialEnergy()
        {
            double sumEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)
            {
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                sumEnergy += this.Struct[i].PotEnergy;
            }

            return sumEnergy;
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        public void Energy(out double potEnergy, out double kinEnergy)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)// Перебор атомов в ячейке
            {
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Struct[i].PotEnergy;
                kinEnergy += this.Struct[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Энергия равновесного состояния (эВ).
        /// </summary>
        /// <param name="temperature">Температура (в Кельвинах).</param>
        /// <param name="numAtoms">Количество атомов структуры.</param>
        /// <returns></returns>
        public static double TemperatureEnergy(double temperature, int numAtoms)
        {
            return (3.0d / 2.0d) * AtomParams.BOLTZMANN * numAtoms * temperature;
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии части структуры.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        /// <param name="startIdx">Начальный индекс атома.</param>
        /// <param name="endIdx">Конечный индекс атома.</param>
        public void Energy(out double potEnergy, out double kinEnergy, int startIdx, int endIdx)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Struct[i].PotEnergy;
                kinEnergy += this.Struct[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Суммирование подсчитанной потенциальной энергии атомов структуры.
        /// </summary>
        /// <returns></returns>
        public double SumPotentialEnergy()
        {
            double potEnergy = 0.0d;

            for (int i = 0; i < this.StructNumAtoms; i++)
                potEnergy += this.Struct[i].PotEnergy;

            return potEnergy;
        }




        /// <summary>
        /// Впомогательный метод для подсчёта атомов для ПКФ в определённой сфере
        /// </summary>
        /// <param name="typeSi">Полученное значение количества атомов кремния</param>
        /// <param name="typeGe">Полученное значение количества атомов германия</param>
        /// <param name="r">Радиус просмотра</param>
        /// <param name="rdr">Толщина сферы</param>
        /// <param name="startIdx">Начальный индекс атома в массиве перебираемых атомов</param>
        /// <param name="endIdx">Конечный индекс атома в массиве перебираемых атомов</param>
        private void CalcAtoms(out int typeSi, out int typeGe, out int typeSn, double r, double rdr, int startIdx, int endIdx)
        {
            typeSi = typeGe = typeSn = 0;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                for (int j = 0; j < StructNumAtoms; j++)
                {
                    if (i == j) continue;

                    double radius = Vector.MagnitudePeriod(Struct[i].Coordinate, Struct[j].Coordinate, L);
                    if (radius > r && radius <= rdr)
                    {
                        if (Struct[j].Type == AtomType.Si) typeSi++;
                        else if (Struct[j].Type == AtomType.Ge) typeGe++;
                        else typeSn++;
                    }
                }
            }


        }
    }
}
