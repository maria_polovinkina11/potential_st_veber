﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibDrawingGraphs;
namespace Potential_St_Veber_v21
{
    public partial class Form1 : Form
    { //Объект класса структуры
        Structure str;

        //Массивы энергий
        double[] kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;

        DrawingGraphs graphPot;
        DrawingGraphs graphKin, graphSum;

        private FormOptions.ModelingParams modelParams;
        Random RandShift, RandNumGen;
        public Form1()
        {
            InitializeComponent();
        }



        //Метод подсчёта количества атомов структуры в зависимости от её размера
        private void numUD_sizeCells_ValueChanged(object sender, EventArgs e)
        {
            int sizeStruct = (int)numUD_sizeCells.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            txt_numAtoms.Text = numAtoms.ToString();
            double value = Math.Round(numAtoms * 3.0 / 100) * 100;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            OutputInfo.Start();
            graphPot = new DrawingGraphs(pict_potEnergy.Width, pict_potEnergy.Height);
            graphKin = new DrawingGraphs(pict_kinEnergy.Width, pict_kinEnergy.Height);
            graphSum = new DrawingGraphs(pict_3.Width, pict_3.Height);

            modelParams = new FormOptions.ModelingParams(0, 0, true);
         
        }

        private void btn_createStructure_Click(object sender, EventArgs e)
        {
            //Проверяем и очищаем файлы Excel 
            bool success0 = OutputInfo.ClearCoordAtomsFile();
            if (!success0)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            bool success1 = OutputInfo.ClearFirstNeighboursdAtoms();
            if (!success1)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            bool success2 = OutputInfo.ClearSecondNeighboursdAtoms();
            if (!success2)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            if (rdBtn_Si.Checked)
            {
                str = new Structure((int)numUD_sizeCells.Value, true, false, false);

            }
            if (rdBtn_Ge.Checked)
            {
                str = new Structure((int)numUD_sizeCells.Value, false, true, false);

            }
            if (rdBtn_Sn.Checked)
            {
                str = new Structure((int)numUD_sizeCells.Value, false, false, true);

            }


            int sizeStruct = (int)numUD_sizeCells.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            for (int i = 0; i < numAtoms; i++)
            {
                OutputInfo.WriteCoordAtomFile(i + 1, str.Struct[i].Coordinate.x / str.StructLatPar, str.Struct[i].Coordinate.y / str.StructLatPar, str.Struct[i].Coordinate.z / str.StructLatPar);

                OutputInfo.WriteFirstNeighboursdAtoms(i + 1,
                     str.Struct[i].FirstNeighbours[0].Index + 1, str.Struct[i].FirstNeighbours[0].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[0].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[0].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[1].Index + 1, str.Struct[i].FirstNeighbours[1].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[1].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[1].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[2].Index + 1, str.Struct[i].FirstNeighbours[2].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[2].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[2].Coordinate.z / str.StructLatPar,
                     str.Struct[i].FirstNeighbours[3].Index + 1, str.Struct[i].FirstNeighbours[3].Coordinate.x / str.StructLatPar, str.Struct[i].FirstNeighbours[3].Coordinate.y / str.StructLatPar, str.Struct[i].FirstNeighbours[3].Coordinate.z / str.StructLatPar);

                OutputInfo.WriteSecondNeighboursdAtoms(i + 1,
                     str.Struct[i].SecondNeighbours[0].Index + 1, str.Struct[i].SecondNeighbours[0].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[0].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[0].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[1].Index + 1, str.Struct[i].SecondNeighbours[1].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[1].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[1].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[2].Index + 1, str.Struct[i].SecondNeighbours[2].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[2].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[2].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[3].Index + 1, str.Struct[i].SecondNeighbours[3].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[3].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[3].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[4].Index + 1, str.Struct[i].SecondNeighbours[4].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[4].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[4].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[5].Index + 1, str.Struct[i].SecondNeighbours[5].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[5].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[5].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[6].Index + 1, str.Struct[i].SecondNeighbours[6].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[6].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[6].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[7].Index + 1, str.Struct[i].SecondNeighbours[7].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[7].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[7].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[8].Index + 1, str.Struct[i].SecondNeighbours[8].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[8].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[8].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[9].Index + 1, str.Struct[i].SecondNeighbours[9].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[9].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[9].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[10].Index + 1, str.Struct[i].SecondNeighbours[10].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[10].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[10].Coordinate.z / str.StructLatPar,
                     str.Struct[i].SecondNeighbours[11].Index + 1, str.Struct[i].SecondNeighbours[11].Coordinate.x / str.StructLatPar, str.Struct[i].SecondNeighbours[11].Coordinate.y / str.StructLatPar, str.Struct[i].SecondNeighbours[11].Coordinate.z / str.StructLatPar);
            }
        }
        private void btn_openCoordAtoms_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenCoordAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btn_openFirstNeigborth_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenFirstNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btn_openSecondNeigbords_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenSecondNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Метод для отрисовки пустых графиков с сеткой
        private void DrawStartGraph(bool bTime)
        {
            string textAxisX;
            if (bTime)
                textAxisX = "Шаг по времени";
            else textAxisX = "Номер шага по времени";


            graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии", true);
            pict_potEnergy.Image = graphPot.GetImage;

            graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии", true);
            pict_kinEnergy.Image = graphKin.GetImage;

            graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий", true);
            pict_3.Image = graphSum.GetImage;

          
        }


        private void btn_calculateEnergy_Click(object sender, EventArgs e)
        {
            
            double energy = str.PotentialEnergy();
            txt_sumEnergy.Text = energy.ToString("f6");
            txt_atomEnergy.Text = (energy / str.StructNumAtoms).ToString("f6");
        }

        private void btn_addStep_Click(object sender, EventArgs e)
        {
            int step = (int)numUD_step.Value;
            if (!listBox_step.Items.Contains(step))
            {
                listBox_step.Items.Add(step);

                if (listBox_step.Items.Count > 1)
                {
                    List<int> list = new List<int>(listBox_step.Items.Count);
                    for (int i = 0; i < listBox_step.Items.Count; i++)
                        list.Add((int)listBox_step.Items[i]);
                    list.Sort();
                    for (int i = 0; i < listBox_step.Items.Count; i++)
                        listBox_step.Items[i] = list[i];
                    list.Clear();
                }

                //перерасчёт времени релаксации
             //   numUD_numStepRelax_ValueChanged(null, null);
            }
        }

        private void btn_deleteStep_Click(object sender, EventArgs e)
        {
            int indx = listBox_step.SelectedIndex;
            if (indx > -1)
            {
                listBox_step.Items.RemoveAt(indx);

                //перерасчёт времени релаксации
             // numUD_numStepRelax_ValueChanged(null, null);
            }
        }

        private void btn_clearStep_Click(object sender, EventArgs e)
        {
            if (listBox_step.Items.Count > 0)
            {
                listBox_step.Items.Clear();

                //перерасчёт времени релаксации
             //   numUD_numStepRelax_ValueChanged(null, null);
            }
        }

        private void button_OpenEnergy_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenEnergyFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

       
        private void DrawGraph(int numStep, bool bTime)
        {
            string textAxisX;

            if (bTime)
            {
                textAxisX = "Шаг по времени";

                graphPot.AddGraph(potEnergy, masTimeEnergy, Color.DarkBlue);
                graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии");
                pict_potEnergy.Image = graphPot.GetImage;

                double energy = Convert.ToDouble(txt_tmprEnergy.Text);
                graphKin.AddAxisX = true;
                graphKin.AddGraph(new double[] { energy, energy }, 0, masTimeEnergy.Max(), Color.Black);
                graphKin.AddGraph(kinEnergy, masTimeEnergy, Color.Red);
                graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии");
                pict_kinEnergy.Image = graphKin.GetImage;

                graphSum.AddAxisX = true;
                graphSum.AddGraph(sumEnergy, masTimeEnergy, Color.DarkGreen);
                graphSum.AddGraph(kinEnergy, masTimeEnergy, Color.Red);
                graphSum.AddGraph(oPotEnergy, masTimeEnergy, Color.DarkBlue);
                graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий");
                pict_3.Image = graphSum.GetImage;
            }
            else
            {
                textAxisX = "Номер шага по времени";

                graphPot.IntValueX = true;
                graphPot.AddGraph(potEnergy, 0, numStep, Color.DarkBlue);
                graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии");
                pict_potEnergy.Image = graphPot.GetImage;
                graphPot.IntValueX = false;

                double energy = Convert.ToDouble(txt_tmprEnergy.Text);
                graphKin.IntValueX = true;
                graphKin.AddAxisX = true;
                graphKin.AddGraph(new double[] { energy, energy }, 0, numStep, Color.Black);
                graphKin.AddGraph(kinEnergy, 0, numStep, Color.Red);
                graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии");
                pict_kinEnergy.Image = graphKin.GetImage;
                graphKin.IntValueX = false;

                graphSum.IntValueX = true;
                graphSum.AddAxisX = true;
                graphSum.AddGraph(sumEnergy, 0, numStep, Color.DarkGreen);
                graphSum.AddGraph(kinEnergy, 0, numStep, Color.Red);
                graphSum.AddGraph(oPotEnergy, 0, numStep, Color.DarkBlue);
                graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий");
                pict_3.Image = graphSum.GetImage;
                graphSum.IntValueX = false;
            }
        }
        private void button_StartRelax_Click(object sender, EventArgs e)
        {
            //Определяем параметры
            int numStep = (int)numUD_numStepRelax.Value;
            double temperature = (double)numUD_tmpr.Value;
            double shift = (double)numUD_shiftAtoms.Value;
            bool normalize = checkBox_startVelocity.Checked;
            int stepNorm = (int)numUD_stepNorm.Value;
            int stepDev = (int)numUD_step.Value;

            graphPot = new DrawingGraphs(pict_potEnergy.Width, pict_potEnergy.Height);
            graphKin = new DrawingGraphs(pict_kinEnergy.Width, pict_kinEnergy.Height);
            graphSum = new DrawingGraphs(pict_3.Width, pict_3.Height);


            //Формируем шаги по времени
            /*   List<int> stepDev = new List<int>(listBox_step.Items.Count);
               for (int i = 0; i < listBox_step.Items.Count; i++)
                   stepDev.Add((int)listBox_step.Items[i]);
            */


            /*    if (modelParams.seedShift != 0)
                    RandShift = new Random(modelParams.seedShift);
                else */
            RandShift = RandNumGen=new Random();

            Relaxation relax = new Relaxation(str);

            FormLoad.MessageHandler handler = delegate
            {
                relax.Initialization(RandNumGen, RandShift, temperature, shift, normalize,
                    stepDev, (double)numUD_multStepRelax.Value , numStep, stepNorm);

                relax.CalculatedTime();
            };

            FormLoad formLoad = new FormLoad("Подготовка данных", handler);
            formLoad.ShowDialog();

            if (MessageBox.Show(String.Format("Примерное время процесса - {0}h {1}m\nПродолжить?", relax.ProcTime.Hours, relax.ProcTime.Minutes),
                "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            //Проверяем и очищаем файлы Excel перед релаксацией
            bool success = OutputInfo.ClearEnergyFile();
            if (!success)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
          

         
            // Релаксация

            if (modelParams.minMainForm)
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
            }

            //Проводим релаксацию
            Task task = Task.Factory.StartNew(delegate { relax.Iterations(); }, TaskCreationOptions.LongRunning);

            FormConsole formConsole = new FormConsole(relax);
            formConsole.ShowDialog();

            if (modelParams.minMainForm)
            {
                this.ShowInTaskbar = true;
                this.WindowState = FormWindowState.Normal;
            }



            handler = delegate
            {
                btn_calculateEnergy_Click(null, null);
               

                //kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;
                kinEnergy = new double[relax.GetMasTimeStep.Length];
                potEnergy = new double[relax.GetMasTimeStep.Length];
                masTimeEnergy = new double[relax.GetMasTimeStep.Length];
                double[] time = new double[relax.GetMasTimeStep.Length];
               
                //Получаем энергию
                sumEnergy = new double[kinEnergy.Length];
                oPotEnergy = new double[kinEnergy.Length];
                for (int i = 0; i < kinEnergy.Length; i++)
                {
                    time[i] = relax.GetMasTimeStep[i];
                    kinEnergy[i] = relax.GetMasKinEnergy[i];
                    potEnergy[i] = relax.GetMasPotEnergy[i];
                    masTimeEnergy[i] = relax.GetMasTimeStep[i];

                    oPotEnergy[i] = potEnergy[i] - potEnergy[0];
                    sumEnergy[i] = oPotEnergy[i] + kinEnergy[i];
                }


                // DrawChart(chart_Ep, potEnergy, time);
                //Отрисовываем
                if (kinEnergy.Length >= 3) DrawGraph(kinEnergy.Length - 1, checkBox_timeRelax.Checked);



            };
            formLoad = new FormLoad("Сохранение данных", handler);
            formLoad.ShowDialog();

            this.Activate();
         

        }

        private void button_OpenEnergy_Click_1(object sender, EventArgs e)
        {

            if (!OutputInfo.OpenEnergyFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

   

        private void rdBtn_Si_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtn_Si.Checked == true)
            {

                txt_latPar.Text = Structure.AtomLatticeParam(true, false, false).ToString();
            }
        }

        private void rdBtn_Ge_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtn_Ge.Checked == true)
            {
                txt_latPar.Text = Structure.AtomLatticeParam(false, true, false).ToString();
            }
        }

        private void rdBtn_Sn_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtn_Sn.Checked == true)
            {
                txt_latPar.Text = Structure.AtomLatticeParam(false, false, true).ToString();
            }
        }



    }
}
