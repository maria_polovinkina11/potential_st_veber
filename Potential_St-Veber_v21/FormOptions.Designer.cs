﻿
namespace Potential_St_Veber_v21
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            this.check_minMainForm = new System.Windows.Forms.CheckBox();
            this.numUD_randShift = new System.Windows.Forms.NumericUpDown();
            this.numUD_randCreate = new System.Windows.Forms.NumericUpDown();
            this.btn_close = new System.Windows.Forms.Button();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randCreate)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Cursor = System.Windows.Forms.Cursors.Help;
            label2.Location = new System.Drawing.Point(13, 55);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(280, 17);
            label2.TabIndex = 9;
            label2.Text = "Затравка для генератора сдвига атомов:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Cursor = System.Windows.Forms.Cursors.Help;
            label1.Location = new System.Drawing.Point(13, 23);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(254, 17);
            label1.TabIndex = 10;
            label1.Text = "Затравка для генератора структуры:";
            // 
            // check_minMainForm
            // 
            this.check_minMainForm.AutoSize = true;
            this.check_minMainForm.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_minMainForm.Checked = true;
            this.check_minMainForm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_minMainForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check_minMainForm.Location = new System.Drawing.Point(13, 96);
            this.check_minMainForm.Margin = new System.Windows.Forms.Padding(4);
            this.check_minMainForm.Name = "check_minMainForm";
            this.check_minMainForm.Size = new System.Drawing.Size(409, 21);
            this.check_minMainForm.TabIndex = 8;
            this.check_minMainForm.Text = "Сворачивать главное окно после запуска моделирования";
            this.check_minMainForm.UseVisualStyleBackColor = true;
            // 
            // numUD_randShift
            // 
            this.numUD_randShift.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_randShift.Location = new System.Drawing.Point(312, 53);
            this.numUD_randShift.Margin = new System.Windows.Forms.Padding(4);
            this.numUD_randShift.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numUD_randShift.Name = "numUD_randShift";
            this.numUD_randShift.Size = new System.Drawing.Size(107, 22);
            this.numUD_randShift.TabIndex = 7;
            // 
            // numUD_randCreate
            // 
            this.numUD_randCreate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_randCreate.Location = new System.Drawing.Point(281, 21);
            this.numUD_randCreate.Margin = new System.Windows.Forms.Padding(4);
            this.numUD_randCreate.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numUD_randCreate.Name = "numUD_randCreate";
            this.numUD_randCreate.Size = new System.Drawing.Size(107, 22);
            this.numUD_randCreate.TabIndex = 6;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.White;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Location = new System.Drawing.Point(369, 196);
            this.btn_close.Margin = new System.Windows.Forms.Padding(4);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 28);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "Закрыть";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 235);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.check_minMainForm);
            this.Controls.Add(this.numUD_randShift);
            this.Controls.Add(this.numUD_randCreate);
            this.Controls.Add(this.btn_close);
            this.Name = "FormOptions";
            this.Text = "FormOptions";
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randCreate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox check_minMainForm;
        private System.Windows.Forms.NumericUpDown numUD_randShift;
        private System.Windows.Forms.NumericUpDown numUD_randCreate;
        private System.Windows.Forms.Button btn_close;
    }
}